from seleniumwire import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import requests
import shutil
from pathlib import Path

import tty, sys, termios

EMAIL = 'name@example.com'
PASSWORD = 'password123'

start_page = 398
last_page = 398

# wait for a keypress in the terminal
def get_keypress():
    orig_settings = termios.tcgetattr(sys.stdin)
    try:
        tty.setcbreak(sys.stdin)
        pressed = sys.stdin.read(1)[0]
    finally:
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)
    return pressed

# sign in to the site
def sign_in(driver):
    driver.find_element_by_css_selector('button.Switch__Button-IusyG.bnWyWE.Button__button-hJkQuQ.ijweQr').click()
    driver.find_element_by_css_selector('button.Button__button-hJkQuQ.gDqDTR').click()
    driver.find_element_by_id('email-field').send_keys(EMAIL)
    driver.find_element_by_id('password-field').send_keys(PASSWORD)
    driver.find_element_by_id('submit-btn').click()

# main

# make the webdriver object, which essentially creates a controllable web browser
driver = webdriver.Firefox()
# always wait for pages to load completely before interacting with them, up to 15s
driver.implicitly_wait(15)

for page_num in range(start_page, last_page + 1):

    # go to page (specific to my book)
    page_url = f"https://bookshelf.vitalsource.com/#/books/9780199339129/cfi/{page_num}!/4/2@100:0.00"
    driver.get(page_url)

    # if we are just starting, the login page will come up, so we have to sign in
    if page_num == start_page:
        sign_in(driver)

    # find the link to the page image itself by going through the HTML
    iframe = driver.find_element_by_xpath("//iframe[@title='Entering Book Content']")
    driver.switch_to.frame(iframe)
    iframe = driver.find_element_by_xpath("//iframe[@title='Book Content']")
    driver.switch_to.frame(iframe)
    # if a captcha comes up, then stop; human enters captcha, presses a key in the terminal, and then the program will continue
    try:
        driver.find_element_by_xpath("//iframe[@id='recaptcha']")
        get_keypress()
    except:
        pass
    # found the link
    dl_link = driver.find_element_by_id('pbk-page').get_attribute('src')

    # delete history to make next part easier
    del driver.requests
    # visit the image page
    driver.get(dl_link)

    # switching from selenium to requests now, need to copy headers&cookies
    # --
    # visiting the page makes a whole lot of HTTP requests; we only need the one that visits the page itself, which, for vitalsource, 
    # happens to have the word "encrypted" in the URL
    request = next(req for req in driver.requests if "encrypted" in req.url)
    hdrs = request.headers
    cookies = driver.get_cookies()
    session = requests.Session()
    for cookie in cookies:
        session.cookies.set(cookie['name'], cookie['value'])

    # download the image with requests
    image = requests.get(dl_link, headers=hdrs, stream=True)

    # copy the downloaded image to the filesystem in a folder called book, using the page number as the filename
    Path("book").mkdir(parents=True, exist_ok=True)
    with open("book/" + str(page_num) + ".jpg", 'wb') as out_file:
        shutil.copyfileobj(image.raw, out_file)
    del image
